import { CounterState } from '../../_model/counter.state.model';
import { Reducer } from 'redux';
import { CounterActionTypes } from './counter.actions.typesafe';

export const counterReducer: Reducer<CounterState> = (prevState = new CounterState(0), action) => {
    switch (action.type) {
        case CounterActionTypes.INCREMENT_REQUEST:
            console.log('test');
            return { ...prevState, loading: true };
        case CounterActionTypes.INCREMENT_ERROR:
            return { ...prevState, loading: false, errors: action.payload };
        case CounterActionTypes.INCREMENT_SUCCESS:
            return { ...prevState, clicksCount: prevState.clicksCount + 1, loading: false };
        default:
            return prevState;
    }
};
