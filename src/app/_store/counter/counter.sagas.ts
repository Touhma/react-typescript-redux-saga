import { callApi } from '../../_utils/apiCalls';
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { CounterActionTypes, incrementError, incrementSuccess } from './counter.actions.typesafe';


const API_ENDPOINT = process.env.REACT_APP_API_ENDPOINT || '';

function* handleCounter() {
    try {
        const res = yield call(callApi, 'get', API_ENDPOINT, '/counter');

        if (res.error) {
            yield put(incrementError(res.error));
        } else {
            yield put(incrementSuccess(res));
        }
    } catch (err) {
        if (err instanceof Error) {
            yield put(incrementError(err.stack!));
        } else {
            yield put(incrementError('An unknown error occured.'));
        }
    }
}

function* watchCounterRequest() {
    yield takeEvery(CounterActionTypes.INCREMENT_REQUEST, handleCounter);
}

export function* counterSaga() {
    yield all([fork(watchCounterRequest)]);
}
