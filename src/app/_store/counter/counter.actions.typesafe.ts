import { action } from 'typesafe-actions';

export class CounterState {
    clicksCount = 0;
    errors = undefined;
    loading = false;
}

export enum CounterActionTypes {
    INCREMENT_REQUEST = '@@counter/INCREMENT_REQUEST',
    INCREMENT_SUCCESS = '@@counter/INCREMENT_SUCCESS',
    INCREMENT_ERROR = '@@counter/INCREMENT_ERROR',
}

export const incrementRequest = () => {
    action(CounterActionTypes.INCREMENT_REQUEST);
};
export const incrementSuccess = (state: CounterState) => action(CounterActionTypes.INCREMENT_SUCCESS, state);
export const incrementError = (message: string) => action(CounterActionTypes.INCREMENT_ERROR, message);

/*

import { Action } from 'redux';
import { action } from 'typesafe-actions';

export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const INCREMENT_IF_ODD = 'INCREMENT_IF_ODD';

interface IncrementCounterAction extends Action {
    type: typeof INCREMENT;
}

interface DecrementCounterAction extends Action {
    type: typeof DECREMENT;
}

interface IncrementIfOddCounterAction extends Action {
    type: typeof INCREMENT_IF_ODD;

}


export type CounterActionTypes = IncrementCounterAction | DecrementCounterAction | IncrementIfOddCounterAction;

*/
