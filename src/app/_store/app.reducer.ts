import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { counterReducer } from './counter/counter.reducer';

import { History } from 'history';

export const createRootReducer = (history: History) =>
    combineReducers({
        counter: counterReducer,
        router: connectRouter(history),
    });
