import React, { Component } from 'react';
import { CounterState } from '../../_model/counter.state.model';
import { ApplicationState } from '../../_model/app.state.model';
import { Dispatch } from 'redux';

import { connect, useDispatch } from 'react-redux';

import * as counterActions from '../../_store/counter/counter.actions.typesafe';
import { incrementRequest } from '../../_store/counter/counter.actions.typesafe';

const initialState = {
    clicksCount: 0,
};

export class CounterButton extends Component<object, CounterState> {
    readonly state: CounterState = initialState;

    render() {
        const { clicksCount } = this.state;
        return (
            <>
                <button onClick={this.handleIncrement}>Increment</button>
                <button onClick={this.handleDecrement}>Decrement</button>
                <p> you clicked on me {clicksCount} times !</p>
            </>
        );
    }

    increment = () => useDispatch(counterActions.incrementRequest());

    private handleIncrement(): void {
        this.increment();
    }

    private handleDecrement(): void {
        console.log('dec ');
    }
}

// It's usually good practice to only include one context at a time in a connected component.
// Although if necessary, you can always include multiple contexts. Just make sure to
// separate them from each other to prevent prop conflicts.
const mapStateToProps = ({ counter }: ApplicationState) => ({
    clicksCount: counter.clicksCount,
});

// mapDispatchToProps is especially useful for constraining our actions to the connected component.
// You can access these via `this.props`.
const mapDispatchToProps = (dispatch: Dispatch) => ({
    fetchRequest: () => dispatch(incrementRequest()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(CounterButton);
