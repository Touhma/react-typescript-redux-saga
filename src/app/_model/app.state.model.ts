import { CounterState } from './counter.state.model';
import { RouterState } from 'connected-react-router';

export interface ApplicationState {
    counter: CounterState;
    router: RouterState;
}
