export class CounterState {
    clicksCount: number;

    constructor(clicksCount: number) {
        this.clicksCount = clicksCount;
    }
}
