import { Store } from 'redux';
import { ApplicationState } from './app.state.model';
import { History } from 'history';

export interface MainProps {
    store: Store<ApplicationState>;
    history: History;
}
