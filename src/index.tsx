import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
import configureStore from './app/_store/app.store.init';

import { createBrowserHistory } from 'history';
// We use hash history because this example is going to be hosted statically.
// Normally you would use browser history.
declare let window: any;

const history = createBrowserHistory();

const initialState = window.INITIAL_REDUX_STATE;
const store = configureStore(history, initialState);

ReactDOM.render(
    <React.StrictMode>
        <App store={store} history={history} />
    </React.StrictMode>,
    document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
