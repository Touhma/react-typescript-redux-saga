import React, { FunctionComponent } from 'react';
import './App.scss';

import { CounterButton } from './app/_feature/counter/counter.component';

import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { MainProps } from './app/_model/app.props.model';

// Create an intersection type of the component props and our Redux props.
// eslint-disable-next-line react/prop-types
export const App: FunctionComponent<MainProps> = ({ store, history }) => {
    return (
        <Provider store={store}>
            <ConnectedRouter history={history}>Todo : Intégrer le routeur</ConnectedRouter>
            <CounterButton />
        </Provider>
    );
};

export default App;
